# api-donneesEntrees

API Données Entrées chargée de sauvegarder les objets d'entrées reçus via le bus de message.

## Module Archivé
Ce module est archivé et fait partie de la V1 du système NumEcoEval.

Ce module a été remplacé par [api-expositiondonneesentrees](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-expositiondonneesentrees)
pour la partie persistance de données d'entrées.
La lecture des données est porté par l'[api-event-donneesentrees](https://gitlab-forge.din.developpement-durable.gouv.fr/pub/numeco/m4g/api-event-donneesentrees).

Les images Docker et le code peuvent encore être consultés, mais **ce code ne sera plus maintenu**.