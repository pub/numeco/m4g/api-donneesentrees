package org.mte.numecoeval.donneesentrees.infrastructure;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.mte.numecoeval.donneesentrees.DonneesEntreesApplication;
import org.mte.numecoeval.donneesentrees.infrastructure.kafka.consumer.KafkaEntreeConsumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(classes = {DonneesEntreesApplication.class})
@EmbeddedKafka(
        topics = { "${numecoeval.kafka.topic.entree.name}"},
        bootstrapServersProperty = "spring.kafka.bootstrap-servers",
        partitions = 1)
@ActiveProfiles(profiles = { "test" })
@Slf4j
class DonneesEntreesApplicationIntegrationTest {
    @Test
    void testContextLoadOK(@Autowired KafkaEntreeConsumer kafkaEntreeConsumer) {
        assertNotNull(kafkaEntreeConsumer);
    }
}
