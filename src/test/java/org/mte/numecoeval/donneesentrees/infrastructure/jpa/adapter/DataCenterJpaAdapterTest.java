package org.mte.numecoeval.donneesentrees.infrastructure.jpa.adapter;

import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.donneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.DataCenterEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository.DataCenterRepository;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapperImpl;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class DataCenterJpaAdapterTest {
    @InjectMocks
    private DataCenterJpaAdapter jpaAdapter;

    @Mock
    DataCenterRepository repository;

    EntreeEntityMapper entreeEntityMapper = new EntreeEntityMapperImpl();

    @BeforeEach
    void setup(){
        jpaAdapter = new DataCenterJpaAdapter(repository, entreeEntityMapper);
    }

    @Test
    void saveShouldConvertAndCallSave() {
        DataCenter domain = Instancio.of(DataCenter.class).create();
        ArgumentCaptor<DataCenterEntity> valueCapture = ArgumentCaptor.forClass(DataCenterEntity.class);

        jpaAdapter.save(domain);

        Mockito.verify(repository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
    }

    @Test
    void saveAllShouldConvertAndCallSaveAll() {
        DataCenter domain1 = Instancio.of(DataCenter.class).create();
        DataCenter domain2 = Instancio.of(DataCenter.class).create();
        ArgumentCaptor<List<DataCenterEntity>> valueCapture = ArgumentCaptor.forClass(List.class);
        List<DataCenter> entrees = Arrays.asList(
                domain1,
                domain2
        );

        jpaAdapter.saveAll(entrees);

        Mockito.verify(repository, times(1)).saveAll(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
        assertEquals(2, valueCapture.getValue().size());
    }
}
