package org.mte.numecoeval.donneesentrees.infrastructure.jpa.adapter;

import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.donneesentrees.domain.model.Application;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.ApplicationEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository.ApplicationRepository;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapperImpl;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class ApplicationJpaAdapterTest {
    @InjectMocks
    private ApplicationJpaAdapter jpaAdapter;

    @Mock
    ApplicationRepository repository;

    EntreeEntityMapper entreeEntityMapper = new EntreeEntityMapperImpl();

    @BeforeEach
    void setup(){
        jpaAdapter = new ApplicationJpaAdapter(repository, entreeEntityMapper);
    }

    @Test
    void saveShouldConvertAndCallSave() {
        Application domain = Instancio.of(Application.class).create();
        ArgumentCaptor<ApplicationEntity> valueCapture = ArgumentCaptor.forClass(ApplicationEntity.class);

        jpaAdapter.save(domain);

        Mockito.verify(repository, times(1)).save(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
    }

    @Test
    void saveAllShouldConvertAndCallSaveAll() {
        Application domain1 = Instancio.of(Application.class).create();
        Application domain2 = Instancio.of(Application.class).create();
        ArgumentCaptor<List<ApplicationEntity>> valueCapture = ArgumentCaptor.forClass(List.class);
        List<Application> entrees = Arrays.asList(
                domain1,
                domain2
        );

        jpaAdapter.saveAll(entrees);

        Mockito.verify(repository, times(1)).saveAll(valueCapture.capture());
        assertNotNull(valueCapture.getValue());
        assertEquals(2, valueCapture.getValue().size());
    }
}
