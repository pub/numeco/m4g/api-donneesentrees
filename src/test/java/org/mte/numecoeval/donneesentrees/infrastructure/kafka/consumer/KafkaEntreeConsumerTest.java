package org.mte.numecoeval.donneesentrees.infrastructure.kafka.consumer;

import org.instancio.Instancio;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mte.numecoeval.donneesentrees.domain.model.Application;
import org.mte.numecoeval.donneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.donneesentrees.domain.model.DonneesEntrees;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.donneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.donneesentrees.domain.port.output.EntreePersistencePort;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeMapper;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeMapperImpl;
import org.mte.numecoeval.topic.data.ApplicationDTO;
import org.mte.numecoeval.topic.data.DonneesEntreeDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EquipementVirtuelDTO;

import java.util.List;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class KafkaEntreeConsumerTest {

    @InjectMocks
    private KafkaEntreeConsumer kafkaEntreeConsumer;

    EntreeMapper entreeMapper = new EntreeMapperImpl();

    @Mock
    private EntreePersistencePort<DonneesEntrees> donneesEntreesEntreePersistencePort;
    @Mock
    private EntreePersistencePort<DataCenter> dataCenterEntreePersistencePort;
    @Mock
    private EntreePersistencePort<EquipementPhysique> equipementPhysiqueEntreePersistencePort;
    @Mock
    private EntreePersistencePort<EquipementVirtuel> equipementVirtuelEntreePersistencePort;
    @Mock
    private EntreePersistencePort<Application> applicationEntreePersistencePort;
    @Mock
    private EntreePersistencePort<Messagerie> messagerieEntreePersistencePort;

    @BeforeEach
    void setup() {
        kafkaEntreeConsumer = new KafkaEntreeConsumer(
                entreeMapper,
                donneesEntreesEntreePersistencePort,
                dataCenterEntreePersistencePort,
                equipementPhysiqueEntreePersistencePort,
                equipementVirtuelEntreePersistencePort,
                applicationEntreePersistencePort,
                messagerieEntreePersistencePort
        );
    }

    @Test
    void nullValueShouldDoNothing() {
        assertDoesNotThrow(() -> kafkaEntreeConsumer.donneesEntreeListener(null));

        Mockito.verify(donneesEntreesEntreePersistencePort, times(0)).saveAll(Mockito.anyList());
        Mockito.verify(dataCenterEntreePersistencePort, times(0)).saveAll(Mockito.anyList());
        Mockito.verify(equipementPhysiqueEntreePersistencePort, times(0)).saveAll(Mockito.anyList());
        Mockito.verify(equipementVirtuelEntreePersistencePort, times(0)).saveAll(Mockito.anyList());
        Mockito.verify(applicationEntreePersistencePort, times(0)).saveAll(Mockito.anyList());
        Mockito.verify(messagerieEntreePersistencePort, times(0)).saveAll(Mockito.anyList());
    }

    @Test
    void givenDonneesEntreesShouldConvertAndCallPersistencePorts() {
        DonneesEntreeDTO donneesEntreeDTO = Instancio.of(DonneesEntreeDTO.class).create();
        int nbrEquipementsVirtuelsOriginal = donneesEntreeDTO.getEquipementsVirtuels().size() +
                donneesEntreeDTO.getEquipementsPhysiques().stream()
                .map(EquipementPhysiqueDTO::getEquipementsVirtuels)
                .mapToInt(List::size)
                .sum();
        int nbrApplicationsOriginal = donneesEntreeDTO.getApplications().size()
                + donneesEntreeDTO.getEquipementsPhysiques().stream()
                        .map(EquipementPhysiqueDTO::getEquipementsVirtuels)
                        .flatMap(List::stream)
                        .map(EquipementVirtuelDTO::getApplications)
                        .mapToInt(List::size)
                        .sum()
                + donneesEntreeDTO.getEquipementsVirtuels().stream()
                        .map(EquipementVirtuelDTO::getApplications)
                        .mapToInt(List::size)
                        .sum()
                ;
        EquipementVirtuelDTO extraVM = Instancio.of(EquipementVirtuelDTO.class).create();
        donneesEntreeDTO.getEquipementsVirtuels().add(extraVM);
        ApplicationDTO extraApplication = Instancio.of(ApplicationDTO.class).create();
        donneesEntreeDTO.getApplications().add(extraApplication);

        ArgumentCaptor<DonneesEntrees> valueCaptureDonneeEntree = ArgumentCaptor.forClass(DonneesEntrees.class);
        ArgumentCaptor<List<DataCenter>> valueCaptureDataCenters = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<EquipementPhysique>> valueCaptureEquipementsPhysiques = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<EquipementVirtuel>> valueCaptureEquipementsVirtuels = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<Application>> valueCaptureApplications = ArgumentCaptor.forClass(List.class);
        ArgumentCaptor<List<Messagerie>> valueCaptureMessageries = ArgumentCaptor.forClass(List.class);

        assertDoesNotThrow(() -> kafkaEntreeConsumer.donneesEntreeListener(donneesEntreeDTO));

        Mockito.verify(donneesEntreesEntreePersistencePort, times(1)).save(valueCaptureDonneeEntree.capture());
        Mockito.verify(dataCenterEntreePersistencePort, times(1)).saveAll(valueCaptureDataCenters.capture());

        DonneesEntrees donneesEntrees = valueCaptureDonneeEntree.getValue();
        assertEquals(donneesEntreeDTO.getDataCenterDtos().size(), donneesEntrees.getNbrDataCenter());
        assertEquals(donneesEntreeDTO.getDataCenterDtos().size(), valueCaptureDataCenters.getValue().size());
        assertTrue(
                valueCaptureDataCenters.getValue().stream().allMatch(domain ->
                        Objects.equals(domain.getDateLot(), donneesEntreeDTO.getDateLot())
                                && Objects.equals(domain.getNomOrganisation(), donneesEntreeDTO.getNomOrganisation())
                )
        );
        Mockito.verify(equipementPhysiqueEntreePersistencePort, times(1)).saveAll(valueCaptureEquipementsPhysiques.capture());
        assertEquals(donneesEntreeDTO.getEquipementsPhysiques().size(), donneesEntrees.getNbrEquipementsPhysiques());
        assertEquals(donneesEntreeDTO.getEquipementsPhysiques().size(), valueCaptureEquipementsPhysiques.getValue().size());
        assertTrue(
                valueCaptureEquipementsPhysiques.getValue().stream().allMatch(domain ->
                        Objects.equals(domain.getDateLot(), donneesEntreeDTO.getDateLot())
                                && Objects.equals(domain.getNomOrganisation(), donneesEntreeDTO.getNomOrganisation())
                )
        );
        Mockito.verify(equipementVirtuelEntreePersistencePort, times(1)).saveAll(valueCaptureEquipementsVirtuels.capture());
        assertEquals(nbrEquipementsVirtuelsOriginal + 1, donneesEntrees.getNbrEquipementsVirtuels());
        assertEquals(nbrEquipementsVirtuelsOriginal + 1, valueCaptureEquipementsVirtuels.getValue().size());
        assertTrue(
                valueCaptureEquipementsVirtuels.getValue().stream().allMatch(domain ->
                        Objects.equals(domain.getDateLot(), donneesEntreeDTO.getDateLot())
                                && Objects.equals(domain.getNomOrganisation(), donneesEntreeDTO.getNomOrganisation())
                )
        );
        Mockito.verify(applicationEntreePersistencePort, times(1)).saveAll(valueCaptureApplications.capture());
        assertEquals(nbrApplicationsOriginal + extraVM.getApplications().size() + 1, donneesEntrees.getNbrApplications());
        assertEquals(nbrApplicationsOriginal + extraVM.getApplications().size() + 1, valueCaptureApplications.getValue().size());
        assertTrue(
                valueCaptureApplications.getValue().stream().allMatch(domain ->
                        Objects.equals(domain.getDateLot(), donneesEntreeDTO.getDateLot())
                                && Objects.equals(domain.getNomOrganisation(), donneesEntreeDTO.getNomOrganisation())
                )
        );
        Mockito.verify(messagerieEntreePersistencePort, times(1)).saveAll(valueCaptureMessageries.capture());
        assertEquals(donneesEntreeDTO.getMessageries().size(), donneesEntrees.getNbrMessageries());
        assertEquals(donneesEntreeDTO.getMessageries().size(), valueCaptureMessageries.getValue().size());
        assertTrue(
                valueCaptureMessageries.getValue().stream().allMatch(domain ->
                        Objects.equals(domain.getDateLot(), donneesEntreeDTO.getDateLot())
                                && Objects.equals(domain.getNomOrganisation(), donneesEntreeDTO.getNomOrganisation())
                )
        );
    }
}
