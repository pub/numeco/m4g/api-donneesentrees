CREATE TABLE IF NOT EXISTS en_donnees_entrees
(
    id                        int8         NOT NULL,
    date_creation             timestamp    NULL,
    date_lot                  date         NULL,
    nom_organisation          varchar(255) NULL,
    nbr_applications          int8         NULL,
    nbr_data_center           int8         NULL,
    nbr_equipements_physiques int8         NULL,
    nbr_equipements_virtuels  int8         NULL,
    nbr_messageries           int8         NULL,
    CONSTRAINT en_donnees_entrees_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_data_center
(
    id                   int8         NOT NULL,
    date_creation        timestamp    NULL,
    date_lot             date         NULL,
    nom_organisation     varchar(255) NULL,
    localisation         varchar(255) NULL,
    nom_court_datacenter varchar(255) NULL,
    nom_entite           varchar(255) NULL,
    nom_long_datacenter  varchar(255) NULL,
    pue                  float8       NULL,
    CONSTRAINT en_data_center_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_equipement_physique
(
    id                        int8         NOT NULL,
    date_creation             timestamp    NULL,
    date_lot                  date         NULL,
    nom_organisation          varchar(255) NULL,
    conso_elec_annuelle       float8       NULL,
    date_achat                date         NULL,
    date_retrait              date         NULL,
    duree_vie_defaut          float8       NULL,
    go_telecharge             float4       NULL,
    modele                    varchar(255) NULL,
    nb_coeur                  varchar(255) NULL,
    nb_jour_utilise_an        float8       NULL,
    nom_court_datacenter      varchar(255) NULL,
    nom_entite                varchar(255) NULL,
    nom_equipement_physique   varchar(255) NULL,
    pays_utilisation          varchar(255) NULL,
    quantite                  float8       NULL,
    serveur                   bool         NOT NULL,
    statut                    varchar(255) NULL,
    "type"                    varchar(255) NULL,
    utilisateur               varchar(255) NULL,
    ref_equipement_par_defaut varchar(255) NULL,
    ref_equipement_retenu     varchar(255) NULL,
    CONSTRAINT en_equipement_physique_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_equipement_virtuel
(
    id                      int8         NOT NULL,
    date_creation           timestamp    NULL,
    date_lot                date         NULL,
    nom_organisation        varchar(255) NULL,
    "cluster"               varchar(255) NULL,
    nom_entite              varchar(255) NULL,
    nom_equipement_physique varchar(255) NULL,
    nom_vm                  varchar(255) NULL,
    vcpu                    int4         NULL,
    CONSTRAINT en_equipement_virtuel_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_application
(
    id                 int8         NOT NULL,
    date_creation      timestamp    NULL,
    date_lot           date         NULL,
    nom_organisation   varchar(255) NULL,
    domaine            varchar(255) NULL,
    nom_application    varchar(255) NULL,
    nom_entite         varchar(255) NULL,
    nom_vm             varchar(255) NULL,
    sous_domaine       varchar(255) NULL,
    type_environnement varchar(255) NULL,
    CONSTRAINT en_application_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS en_messagerie
(
    id                             int8         NOT NULL,
    date_creation                  timestamp    NULL,
    date_lot                       date         NULL,
    nom_organisation               varchar(255) NULL,
    mois_annee                     int4         NULL,
    nom_entite                     varchar(255) NULL,
    nombre_mail_emis               int4         NULL,
    nombre_mail_emisxdestinataires int4         NULL,
    volume_total_mail_emis         int4         NULL,
    CONSTRAINT en_messagerie_pkey PRIMARY KEY (id)
);

