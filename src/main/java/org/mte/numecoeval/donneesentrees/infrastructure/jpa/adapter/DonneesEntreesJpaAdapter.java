package org.mte.numecoeval.donneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.mte.numecoeval.donneesentrees.domain.model.DonneesEntrees;
import org.mte.numecoeval.donneesentrees.domain.port.output.EntreePersistencePort;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository.DonneesEntreesRepository;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class DonneesEntreesJpaAdapter implements EntreePersistencePort<DonneesEntrees> {

    DonneesEntreesRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(DonneesEntrees entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<DonneesEntrees> entrees) {
        CollectionUtils.emptyIfNull(entrees).forEach(this::save);
    }
}
