package org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.MappedSuperclass;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.CreationTimestamp;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@MappedSuperclass
public abstract class AbstractEntreeEntity {

    @CreationTimestamp
    protected LocalDateTime dateCreation;

    /**
     * Nom de l'organisation liée aux données - Metadata
     */
    protected String nomOrganisation;

    /**
     * La date du lot permet d’agréger les données provenant de différentes sources et d'en faire un suivi temporel
     */
    protected LocalDate dateLot;
}
