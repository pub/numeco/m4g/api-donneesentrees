package org.mte.numecoeval.donneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.donneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.donneesentrees.domain.port.output.EntreePersistencePort;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository.MessagerieRepository;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class MessagerieJpaAdapter implements EntreePersistencePort<Messagerie> {

    MessagerieRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(Messagerie entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<Messagerie> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListMessagerie(entrees));
    }
}
