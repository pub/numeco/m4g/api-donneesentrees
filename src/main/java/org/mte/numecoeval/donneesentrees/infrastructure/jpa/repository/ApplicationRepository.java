package org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository;

import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.ApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<ApplicationEntity,Long>
{
}
