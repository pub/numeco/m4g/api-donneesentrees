package org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_DATA_CENTER")
@Entity
public class DataCenterEntity extends AbstractEntreeEntity {

    @Id
    @GeneratedValue(generator = "SEQ_EN_DATA_CENTER", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_DATA_CENTER", sequenceName="SEQ_EN_DATA_CENTER",allocationSize=1000)
    @Column(nullable = false)
    protected Long id;

    private String nomCourtDatacenter;
    private String nomLongDatacenter;
    private Double pue;
    private String localisation;
    private String nomEntite;


}
