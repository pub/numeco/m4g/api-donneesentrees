package org.mte.numecoeval.donneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.donneesentrees.domain.model.Application;
import org.mte.numecoeval.donneesentrees.domain.port.output.EntreePersistencePort;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository.ApplicationRepository;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ApplicationJpaAdapter implements EntreePersistencePort<Application>
{
    private ApplicationRepository repository;
    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(Application entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<Application> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListApplication(entrees) );
    }
}