package org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_MESSAGERIE")
@Entity
public class MessagerieEntity extends AbstractEntreeEntity
{
    @Id
    @GeneratedValue(generator = "SEQ_EN_MESSAGERIE", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_MESSAGERIE", sequenceName="SEQ_EN_MESSAGERIE",allocationSize=1000)
    @Column(nullable = false)
    private Long id;
    private Integer volumeTotalMailEmis;
    private Integer nombreMailEmis;
    private Integer nombreMailEmisXDestinataires;
    private Integer moisAnnee;
    private String nomEntite;


}
