package org.mte.numecoeval.donneesentrees.infrastructure.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.donneesentrees.domain.model.Application;
import org.mte.numecoeval.donneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.donneesentrees.domain.model.DonneesEntrees;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.donneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.ApplicationEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.DataCenterEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.DonneesEntreesEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.EquipementPhysiqueEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.EquipementVirtuelEntity;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity.MessagerieEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EntreeEntityMapper {

    DonneesEntreesEntity toEntity(DonneesEntrees domain);
    DataCenterEntity toEntity(DataCenter domain);
    EquipementPhysiqueEntity toEntity(EquipementPhysique domain);
    @Mapping(target = "vCPU", source = "VCPU")
    EquipementVirtuelEntity toEntity(EquipementVirtuel domain);
    ApplicationEntity toEntity(Application domain);
    MessagerieEntity toEntity(Messagerie domain);
    List<DataCenterEntity> toEntityListDataCenter(List<DataCenter> domains);
    List<EquipementPhysiqueEntity> toEntityListEquipementPhysique(List<EquipementPhysique> domains);
    List<EquipementVirtuelEntity> toEntityListEquipementVirtuel(List<EquipementVirtuel> domains);
    List<ApplicationEntity> toEntityListApplication(List<Application> domains);
    List<MessagerieEntity> toEntityListMessagerie(List<Messagerie> domains);

    DonneesEntrees toDomain(DonneesEntreesEntity entity);
    DataCenter toDomain(DataCenterEntity entity);
    EquipementPhysique toDomain(EquipementPhysiqueEntity entity);
    EquipementVirtuel toDomain(EquipementVirtuelEntity entity);
    Application toDomain(ApplicationEntity entity);
    Messagerie toDomain(MessagerieEntity entity);
    List<DataCenter> toDomainListDataCenter(List<DataCenterEntity> entities);
    List<EquipementPhysique> toDomainListEquipementPhysique(List<EquipementPhysiqueEntity> entities);
    List<EquipementVirtuel> toDomainListEquipementVirtuel(List<EquipementVirtuelEntity> entities);
    List<Application> toDomainListApplication(List<ApplicationEntity> entities);
    List<Messagerie> toDomainListMessagerie(List<MessagerieEntity> entities);
}
