package org.mte.numecoeval.donneesentrees.infrastructure.jpa.adapter;

import lombok.AllArgsConstructor;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.donneesentrees.domain.port.output.EntreePersistencePort;
import org.mte.numecoeval.donneesentrees.infrastructure.jpa.repository.EquipementVirtuelRepository;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeEntityMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class EquipementVirtuelJpaAdapter implements EntreePersistencePort<EquipementVirtuel> {

    EquipementVirtuelRepository repository;

    EntreeEntityMapper entreeEntityMapper;

    @Override
    public void save(EquipementVirtuel entree) {
        repository.save(entreeEntityMapper.toEntity(entree));
    }

    @Override
    public void saveAll(List<EquipementVirtuel> entrees) {
        repository.saveAll(entreeEntityMapper.toEntityListEquipementVirtuel(entrees) );
    }
}
