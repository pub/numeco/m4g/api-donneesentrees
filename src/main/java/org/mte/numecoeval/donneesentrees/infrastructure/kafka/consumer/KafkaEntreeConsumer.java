package org.mte.numecoeval.donneesentrees.infrastructure.kafka.consumer;

import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.mte.numecoeval.donneesentrees.domain.model.Application;
import org.mte.numecoeval.donneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.donneesentrees.domain.model.DonneesEntrees;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.donneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.donneesentrees.domain.port.output.EntreePersistencePort;
import org.mte.numecoeval.donneesentrees.infrastructure.mapper.EntreeMapper;
import org.mte.numecoeval.topic.data.DonneesEntreeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

@Component
@AllArgsConstructor
public class KafkaEntreeConsumer {

    private  static  final Logger LOG = LoggerFactory.getLogger(KafkaEntreeConsumer.class);

    private EntreeMapper entreeMapper;
    private EntreePersistencePort<DonneesEntrees> donneesEntreesEntreePersistencePort;
    private EntreePersistencePort<DataCenter> dataCenterEntreePersistencePort;
    private EntreePersistencePort<EquipementPhysique> equipementPhysiqueEntreePersistencePort;
    private EntreePersistencePort<EquipementVirtuel> equipementVirtuelEntreePersistencePort;
    private EntreePersistencePort<Application> applicationEntreePersistencePort;
    private EntreePersistencePort<Messagerie> messagerieEntreePersistencePort;


    @KafkaListener(topics = "${numecoeval.kafka.topic.entree.name}", containerFactory = "consumerDonneesEntreeDTOFactory")
    public void donneesEntreeListener(DonneesEntreeDTO donneesEntreeDTO){
        if(donneesEntreeDTO == null) {
            LOG.warn("Message null reçu sur le topic des données d'entrées");
            return;
        }

        LOG.info("Message reçu  : Date de lot : {} - Nom Organisation : {}",
                donneesEntreeDTO.getDateLot(), donneesEntreeDTO.getNomOrganisation()
        );
        var listEquipementsVirtuels = new ArrayList<>(CollectionUtils.emptyIfNull(donneesEntreeDTO.getEquipementsVirtuels()));
        var listEquipementsApplications = new ArrayList<>(CollectionUtils.emptyIfNull(donneesEntreeDTO.getApplications()));

        // Complétions des listes globales
        // Les objets sous-jacents (équipements virtuels et applications) n'arrivent que depuis les équipements physiques.
        // Ces boucles permettent de simplement conserver une seule liste unique d'objets en plus de ceux portés par DonneesEntreeDTO.
        CollectionUtils.emptyIfNull(donneesEntreeDTO.getEquipementsPhysiques()).forEach(equipementPhysiqueDTO ->
            listEquipementsVirtuels.addAll(equipementPhysiqueDTO.getEquipementsVirtuels())
        );
        listEquipementsVirtuels.forEach(equipementVirtuelDTO ->
            listEquipementsApplications.addAll(equipementVirtuelDTO.getApplications())
        );

        StopWatch globalStopWatch = StopWatch.createStarted();

        StopWatch stopWatch = StopWatch.createStarted();
        DonneesEntrees donneesEntrees = entreeMapper.toDomain(donneesEntreeDTO);
        // Mise à jour du nombre réels d'objets applications & équipements virtuels
        donneesEntrees.setNbrEquipementsVirtuels((long) listEquipementsVirtuels.size());
        donneesEntrees.setNbrApplications((long) listEquipementsApplications.size());
        donneesEntreesEntreePersistencePort.save(
                donneesEntrees
        );
        stopWatch.stop();
        LOG.info("Fin du traitement de l'objet DonneesEntreeDTO reçue en {} secondes",
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        dataCenterEntreePersistencePort.saveAll(
                CollectionUtils.emptyIfNull(donneesEntreeDTO.getDataCenterDtos()).stream()
                        .map(dto -> entreeMapper.toDomain(dto, donneesEntreeDTO))
                        .toList()
        );
        stopWatch.stop();
        LOG.info("Fin du traitement des {} objets DataCenter reçus en {} secondes",
                CollectionUtils.size(donneesEntreeDTO.getDataCenterDtos()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        equipementPhysiqueEntreePersistencePort.saveAll(
                CollectionUtils.emptyIfNull(donneesEntreeDTO.getEquipementsPhysiques()).stream()
                        .map(dto -> entreeMapper.toDomain(dto, donneesEntreeDTO))
                        .toList()
        );
        stopWatch.stop();
        LOG.info("Fin du traitement des {} objets EquipementPhysique reçus en {} secondes",
                CollectionUtils.size(donneesEntreeDTO.getEquipementsPhysiques()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        equipementVirtuelEntreePersistencePort.saveAll(
                CollectionUtils.emptyIfNull(listEquipementsVirtuels).stream()
                        .map(dto -> entreeMapper.toDomain(dto, donneesEntreeDTO))
                        .toList()
        );
        stopWatch.stop();
        LOG.info("Fin du traitement des {} objets EquipementVirtuel reçus en {} secondes",
                listEquipementsVirtuels.size(),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        applicationEntreePersistencePort.saveAll(
                CollectionUtils.emptyIfNull(listEquipementsApplications).stream()
                        .map(dto -> entreeMapper.toDomain(dto, donneesEntreeDTO))
                        .toList()
        );
        stopWatch.stop();
        LOG.info("Fin du traitement des {} objets Applications reçus en {} secondes",
                listEquipementsApplications.size(),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        stopWatch = StopWatch.createStarted();
        messagerieEntreePersistencePort.saveAll(
                CollectionUtils.emptyIfNull(donneesEntreeDTO.getMessageries()).stream()
                        .map(dto -> entreeMapper.toDomain(dto, donneesEntreeDTO))
                        .toList()
        );
        stopWatch.stop();
        LOG.info("Fin du traitement des {} objets Messagerie reçus en {} secondes",
                CollectionUtils.size(donneesEntreeDTO.getMessageries()),
                stopWatch.getTime(TimeUnit.SECONDS)
        );

        globalStopWatch.stop();
        LOG.info("Fin du traitement du message reçu (Date de lot : {} - Nom Organisation : {}) en {} secondes",
                donneesEntreeDTO.getDateLot(), donneesEntreeDTO.getNomOrganisation(),
                globalStopWatch.getTime(TimeUnit.SECONDS)
        );
    }
}
