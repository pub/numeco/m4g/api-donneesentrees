package org.mte.numecoeval.donneesentrees.infrastructure.mapper;

import org.apache.commons.collections4.CollectionUtils;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mte.numecoeval.donneesentrees.domain.model.Application;
import org.mte.numecoeval.donneesentrees.domain.model.DataCenter;
import org.mte.numecoeval.donneesentrees.domain.model.DonneesEntrees;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementPhysique;
import org.mte.numecoeval.donneesentrees.domain.model.EquipementVirtuel;
import org.mte.numecoeval.donneesentrees.domain.model.Messagerie;
import org.mte.numecoeval.topic.data.ApplicationDTO;
import org.mte.numecoeval.topic.data.DataCenterDTO;
import org.mte.numecoeval.topic.data.DonneesEntreeDTO;
import org.mte.numecoeval.topic.data.EquipementPhysiqueDTO;
import org.mte.numecoeval.topic.data.EquipementVirtuelDTO;
import org.mte.numecoeval.topic.data.MessagerieDTO;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EntreeMapper {

    @Mapping(expression = "java(getSizeList(dto.getDataCenterDtos()))",target = "nbrDataCenter")
    @Mapping(expression = "java(getSizeList(dto.getEquipementsPhysiques()))", target = "nbrEquipementsPhysiques")
    @Mapping(expression = "java(getSizeList(dto.getEquipementsVirtuels()))", target = "nbrEquipementsVirtuels")
    @Mapping(expression = "java(getSizeList(dto.getApplications()))", target = "nbrApplications")
    @Mapping(expression = "java(getSizeList(dto.getMessageries()))", target = "nbrMessageries")
    DonneesEntrees toDomain(DonneesEntreeDTO dto);
    @Mapping(target = "id", source = "id", ignore = true)
    DataCenter toDomain(DataCenterDTO dto);
    @Mapping(target = "id", source = "dto.id", ignore = true)
    @Mapping(target = "nomOrganisation", source = "donneesEntree.nomOrganisation")
    @Mapping(target = "dateLot", source = "donneesEntree.dateLot")
    DataCenter toDomain(DataCenterDTO dto, DonneesEntreeDTO donneesEntree);
    @Mapping(target = "id", source = "id", ignore = true)
    EquipementPhysique toDomain(EquipementPhysiqueDTO dto);

    @Mapping(target = "id", source = "dto.id", ignore = true)
    @Mapping(target = "nomOrganisation", source = "donneesEntree.nomOrganisation")
    @Mapping(target = "dateLot", source = "donneesEntree.dateLot")
    EquipementPhysique toDomain(EquipementPhysiqueDTO dto, DonneesEntreeDTO donneesEntree);

    @Mapping(target = "id", source = "id", ignore = true)
    EquipementVirtuel toDomain(EquipementVirtuelDTO dto);

    @Mapping(target = "id", source = "dto.id", ignore = true)
    @Mapping(target = "nomOrganisation", source = "donneesEntree.nomOrganisation")
    @Mapping(target = "dateLot", source = "donneesEntree.dateLot")
    EquipementVirtuel toDomain(EquipementVirtuelDTO dto, DonneesEntreeDTO donneesEntree);
    @Mapping(target = "id", source = "id", ignore = true)
    Application toDomain(ApplicationDTO dto);

    @Mapping(target = "id", source = "dto.id", ignore = true)
    @Mapping(target = "nomOrganisation", source = "donneesEntree.nomOrganisation")
    @Mapping(target = "dateLot", source = "donneesEntree.dateLot")
    Application toDomain(ApplicationDTO dto, DonneesEntreeDTO donneesEntree);
    @Mapping(target = "id", source = "id", ignore = true)
    Messagerie toDomain(MessagerieDTO dto);

    @Mapping(target = "id", source = "dto.id", ignore = true)
    @Mapping(target = "nomOrganisation", source = "donneesEntree.nomOrganisation")
    @Mapping(target = "dateLot", source = "donneesEntree.dateLot")
    Messagerie toDomain(MessagerieDTO dto, DonneesEntreeDTO donneesEntree);

    /**
     * Méthode utilitaire pour obtenir la taille d'une liste.
     * Préserve la valeur {@code null} quand la liste vaut {@code null}.
     * @param list liste à traiter
     * @return {@code null} si la liste est {@code null} sinon la taille de la liste
     */
    default Long getSizeList(List<?> list) {
        if(list == null) {
            return null;
        }

        return (long) CollectionUtils.size(list);
    }


}
