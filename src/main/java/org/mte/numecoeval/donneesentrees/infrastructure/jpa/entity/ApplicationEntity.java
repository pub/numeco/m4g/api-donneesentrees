package org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Entité représentant une application dans les données d'entrées.
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "EN_APPLICATION")
@Entity
public class ApplicationEntity extends AbstractEntreeEntity{
   @Id
   @GeneratedValue(generator = "SEQ_EN_APPLICATION", strategy = GenerationType.SEQUENCE)
   @SequenceGenerator(name = "SEQ_EN_APPLICATION", sequenceName="SEQ_EN_APPLICATION",allocationSize=1000)
   @Column(nullable = false)
   private Long id;

   /**
    * Nom de l'application
    */
   private String nomApplication;

   /**
    * Type d'environnement de l'instance de l'application
    */
   private String typeEnvironnement;

   /**
    * Référence de l'équipement virtuel rattaché
    */
   @Column(name = "nom_vm")
   private String nomVM;

   /**
    * Domaine ou catégorie principale de l'application
    */
   private String domaine;

   /**
    * Domaine ou catégorie secondaire de l'application
    */
   private String sousDomaine;

   /**
    * Nom de l'entité rattachée à l'application
    */
   private String nomEntite;
}
