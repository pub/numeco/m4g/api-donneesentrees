package org.mte.numecoeval.donneesentrees.infrastructure.jpa.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.SequenceGenerator;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

/**
 * Entité représentant un message global d'arrivée de Données d'entrées.
 */
@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "EN_DONNEES_ENTREES")
@Entity
public class DonneesEntreesEntity extends AbstractEntreeEntity {
    @Id
    @GeneratedValue(generator = "SEQ_EN_DONNEES_ENTREES", strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "SEQ_EN_DONNEES_ENTREES", sequenceName = "SEQ_EN_DONNEES_ENTREES", allocationSize = 1000)
    @Column(nullable = false)
    protected Long id;

    /**
     * Nombre de Data Centers rattachés à ce message de données d'entrées
     */
    private Long nbrDataCenter;

    /**
     * Nombre d'équipments physiques rattachés à ce message de données d'entrées
     */
    private Long nbrEquipementsPhysiques;

    /**
     * Nombre d'équipements virtuels rattachés à ce message de données d'entrées
     */
    private Long nbrEquipementsVirtuels;

    /**
     * Nombre d'applications rattachés à ce message de données d'entrées
     */
    private Long nbrApplications;

    /**
     * Nombre d'éléments de messageries rattachés à ce message de données d'entrées
     */
    private Long nbrMessageries;

}
