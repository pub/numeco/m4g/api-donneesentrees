package org.mte.numecoeval.donneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Accessors(chain = true)
public class DataCenter extends AbstractEntree{
    String nomCourtDatacenter;
    String nomLongDatacenter;
    Double pue;
    String localisation;
    String nomEntite;
}
