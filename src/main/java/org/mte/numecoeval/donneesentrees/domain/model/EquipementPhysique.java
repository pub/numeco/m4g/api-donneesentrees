package org.mte.numecoeval.donneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDate;
@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Accessors(chain = true)
public class EquipementPhysique extends AbstractEntree {
    String nomEquipementPhysique;
    String type;
    String modele;
    String statut;
    String paysDUtilisation;
    String utilisateur;
    LocalDate dateAchat;
    LocalDate dateRetrait;
    String nbCoeur;
    String nomCourtDatacenter;
    Double nbJourUtiliseAn;
    Float goTelecharge;
    Double consoElecAnnuelle;
    boolean serveur;
    Double dureeVieDefaut;
    // référence d'équipement par défaut, propre au traitement
    String refEquipementParDefaut;
    // référence d'équipement retenu via Correspondance dans le référentiel, propre au traitement
    String refEquipementRetenu;
    String nomEntite;
    Double quantite;
}
