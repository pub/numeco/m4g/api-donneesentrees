package org.mte.numecoeval.donneesentrees.domain.port.output;

import org.mte.numecoeval.donneesentrees.domain.model.AbstractEntree;

import java.util.List;

public interface EntreePersistencePort<T extends AbstractEntree> {
    void save(T entree);
    void saveAll(List<T> entrees);
}
