package org.mte.numecoeval.donneesentrees.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@SuperBuilder
@NoArgsConstructor
public abstract class AbstractEntree {

    protected Long id;
    protected LocalDate dateCreation;
    private String nomOrganisation;
    private LocalDate dateLot;

}
