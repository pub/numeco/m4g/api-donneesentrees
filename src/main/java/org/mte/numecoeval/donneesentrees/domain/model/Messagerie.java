package org.mte.numecoeval.donneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Accessors(chain = true)
public class Messagerie extends AbstractEntree
{
    private Integer volumeTotalMailEmis;
    private Integer nombreMailEmis;
    private Integer nombreMailEmisXDestinataires;
    private Integer moisAnnee;
    private String nomEntite;
}
