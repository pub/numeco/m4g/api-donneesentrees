package org.mte.numecoeval.donneesentrees.domain.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Accessors(chain = true)
public class DonneesEntrees extends AbstractEntree {
    private Long nbrDataCenter;
    private Long nbrEquipementsPhysiques;
    private Long nbrEquipementsVirtuels;
    private Long nbrApplications;
    private Long nbrMessageries;


}
