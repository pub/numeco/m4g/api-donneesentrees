package org.mte.numecoeval.donneesentrees.domain.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@SuperBuilder
@Accessors(chain = true)
public class Application extends AbstractEntree{
    String nomApplication;
    String typeEnvironnement;
    String nomVM;
    String domaine;
    String sousDomaine;
    String nomEntite;

}
