package org.mte.numecoeval.donneesentrees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DonneesEntreesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DonneesEntreesApplication.class, args);
	}

}
